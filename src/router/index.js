import { createRouter, createWebHistory } from 'vue-router'

function loadView(view) {
  return () => import(`@/views/${view}.vue`)
}

const routes = [
  {
    path: '/',
    name: 'Index',
    component: loadView('Index'),
    meta: {
      layout: 'main',
      title: '工作台',
      requireAuth: true
    }
  },
  // {
  //   path: '*',
  //   name: '404',
  //   component: loadView('404'),
  //   meta: {
  //     layout: 'container',
  //     title: '404'
  //   }
  // },
  {
    path: '/index',
    name: 'Work',
    component: loadView('Index'),
    meta: {
      layout: 'main',
      title: '工作台',
      requireAuth: true
    }
  },
  {
    path: '/auth/login',
    name: 'Login',
    component: loadView('auth/Login'),
    meta: {
      layout: 'container',
      title: '统一登录中心'
    }
  },
  {
    path: '/auth/retrieve',
    name: 'Retrieve',
    component: loadView('auth/Retrieve'),
    meta: {
      layout: 'container',
      title: '找回密码'
    }
  },
  {
    path: '/about',
    name: 'About',
    component: loadView('About'),
    meta: {
      layout: 'default',
      title: '系统简介'
    }
  }
]

const router = createRouter({
  base: '/',
  mode: 'history',
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
