import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './registerServiceWorker'
import router from './router'
import store from './store'
import AppConfig from './config/App'
router.beforeEach((to,form,next) => {
    document.title = (to.meta.title)?to.meta.title + ' - ' + AppConfig.app_name:AppConfig.app_name;
    // if(to.meta.requireAuth) {
    //     var hasLogin = sessionStorage.getItem("hasLogin");
    //     if(hasLogin == true) {
    //         next();
    //     } else {
    //         console.log(to.path);
    //         next({
    //             path: '/auth/login',
    //             params: { callback_url: encodeURIComponent(to.path) }
    //         });
    //     }
    // } else {
        next();
    // }
})
import DefaultLayout from './layout/default'
import MainLayout from './layout/main'
import ContainerLayout from './layout/container'
createApp(App).component('default-layout',DefaultLayout)
.component('main-layout',MainLayout)
.component('container-layout',ContainerLayout)
.use(store).use(ElementPlus).use(router).mount('#app')

import './assets/icon/iconfont.js'
import './assets/icon/iconfont.css'
